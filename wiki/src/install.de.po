# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2022-08-26 15:01+0000\n"
"PO-Revision-Date: 2022-12-20 10:06+0000\n"
"Last-Translator: xin <xin@riseup.net>\n"
"Language-Team: \n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.9.1\n"

#. type: Content of: <div>
#, fuzzy
#| msgid "[[!meta title=\"Download and install Tails\"]]"
msgid "[[!meta title=\"Install Tails\"]]"
msgstr "[[!meta title=\"Herunterladen und Installieren von Tails\"]]"

#. type: Content of: outside any tag (error?)
#, fuzzy
#| msgid ""
#| "[[!meta title=\"Welcome to the Tails Installation Assistant\"]] [[!meta "
#| "stylesheet=\"bootstrap.min\" rel=\"stylesheet\" title=\"\"]] [[!meta "
#| "stylesheet=\"inc/stylesheets/assistant\" rel=\"stylesheet\" title=\"\"]] "
#| "[[!meta stylesheet=\"inc/stylesheets/router-install\" rel=\"stylesheet\" "
#| "title=\"\"]] [[!inline pages=\"install/inc/tails-installation-assistant."
#| "inline\" raw=\"yes\" sort=\"age\"]]"
msgid ""
"[[!meta stylesheet=\"hide-breadcrumbs\" rel=\"stylesheet\" title=\"\"]] [[!"
"meta stylesheet=\"install\" rel=\"stylesheet\" title=\"\"]]"
msgstr ""
"[[!meta title=\"Willkommen beim Tails Installations-Assistenten\"]] [[!meta "
"stylesheet=\"bootstrap\" rel=\"stylesheet\" title=\"\"]] [[!meta stylesheet="
"\"inc/stylesheets/assistant\" rel=\"stylesheet\" title=\"\"]] [[!meta "
"stylesheet=\"inc/stylesheets/router-install\" rel=\"stylesheet\" title="
"\"\"]] [[!inline pages=\"install/inc/tails-installation-assistant.inline.de"
"\" raw=\"yes\" sort=\"age\"]]"

#. type: Content of: <div><div>
msgid "[[ [[!img install/inc/icons/windows.png link=\"no\" alt=\"\"]]"
msgstr ""

#. type: Content of: <div><div><p>
msgid "Windows"
msgstr "Windows"

#. type: Content of: <div><div>
#, fuzzy
#| msgid "|install/win]]"
msgid "|install/windows]]"
msgstr "|install/win]]"

#. type: Content of: <div><div>
msgid "[[ [[!img install/inc/icons/apple.png link=\"no\" alt=\"\"]]"
msgstr ""

#. type: Content of: <div><div><p>
msgid "macOS"
msgstr "macOS"

#. type: Content of: <div><div>
msgid "|install/mac]]"
msgstr "|install/mac]]"

#. type: Content of: <div><div>
msgid "[[ [[!img install/inc/icons/linux.png link=\"no\" alt=\"\"]]"
msgstr ""

#. type: Content of: <div><div><p>
#, fuzzy
#| msgid "Other Linux"
msgid "Linux"
msgstr "Anderes Linux"

#. type: Content of: <div><div>
msgid "|install/linux]]"
msgstr "|install/linux]]"

#. type: Content of: <div><div>
msgid "[[ [[!img install/inc/icons/expert.png link=\"no\" alt=\"\"]]"
msgstr ""

#. type: Content of: <div><div><p>
msgid "Terminal"
msgstr ""

#. type: Content of: <div><div><p>
msgid "Debian or Ubuntu using the command line and GnuPG"
msgstr ""

#. type: Content of: <div><div>
#, fuzzy
#| msgid "|install/linux]]"
msgid "|install/expert]]"
msgstr "|install/linux]]"

#. type: Content of: <div><p>
msgid ""
"If you know someone you trust who uses Tails already, you can install your "
"Tails by cloning their Tails:"
msgstr ""

#. type: Content of: <div><ul><li>
msgid "[[Install by cloning from another Tails on PC|install/clone/pc]]"
msgstr ""

#. type: Content of: <div><ul><li>
msgid "[[Install by cloning from another Tails on Mac|install/clone/mac]]"
msgstr ""

#. type: Content of: <h3>
msgid "Other options:"
msgstr ""

#. type: Content of: <ul><li>
msgid "[[Download only (for USB sticks)|install/download]]"
msgstr ""

#. type: Content of: <ul><li>
msgid "[[Burn a Tails DVD|install/dvd]]"
msgstr ""

#. type: Content of: <ul><li>
msgid "[[Run Tails in a virtual machine|install/vm]]"
msgstr ""

#. type: Content of: <div><p>
msgid ""
"<strong>Tails doesn't work on smartphones or tablets.</strong> The hardware "
"of smartphones and tablets is very different from the hardware of "
"computers.  For now, it's impossible to make smartphone and tablet hardware "
"work with Linux distributions like Tails."
msgstr ""

#~ msgid "Thank you for your interest in Tails."
#~ msgstr "Danke für Ihr Interesse an Tails."

#~ msgid ""
#~ "Installing Tails can be quite long but we hope you will still have a good "
#~ "time :)"
#~ msgstr ""
#~ "Tails zu installieren kann recht lange dauern, wir hoffen jedoch, dass "
#~ "Sie eine schöne Zeit haben :)"

#~ msgid ""
#~ "We will first ask you a few questions to choose your installation "
#~ "scenario and then guide you step by step."
#~ msgstr ""
#~ "Wir werden Ihnen zuerst ein paar Fragen stellen, um Ihr "
#~ "Installationsszenarium auszuwählen, und Sie dann Schritt für Schritt "
#~ "anleiten."

#~ msgid "Which operating system are you installing Tails from?"
#~ msgstr "Von welchem Betriebssystem aus installieren Sie Tails?"

#~ msgid "[["
#~ msgstr "[["

#~ msgid "|install/debian]]"
#~ msgstr "|install/debian]]"

#~ msgid "<small>(Red Hat, Fedora, etc.)</small>"
#~ msgstr "<small>(Red Hat, Fedora, usw.)</small>"

#~ msgid "Let's start the journey!"
#~ msgstr "Lassen Sie uns die Reise beginnen!"

#~ msgid "Welcome to the"
#~ msgstr "Willkommen beim"

#~ msgid "<strong>Tails Installation Assistant</strong>"
#~ msgstr "<strong>Tails Installations-Assistenten</strong>"

#~ msgid ""
#~ "The following set of instructions is quite new. If you face problems "
#~ "following them:"
#~ msgstr ""
#~ "Die folgenden Anweisungen sind noch recht neu. Falls Sie Probleme bei der "
#~ "Durchführung haben:"

#~ msgid "[[Report your problem.|support/talk]]"
#~ msgstr "[[Berichten Sie Ihr Problem.|support/talk]]"

#~ msgid ""
#~ "Try following our old instructions for [[downloading|install]] or "
#~ "[[installing|doc/first_steps]] instead."
#~ msgstr ""
#~ "Versuchen Sie stattdessen unsere alten Anleitungen zum [[Herunterladen|/"
#~ "download]] oder [[Installieren|doc/first_steps]]."
